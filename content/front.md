+++
+++

# Rust Belt Rust 2018, Ann Arbor, October 19 & 20

![Rust Belt Rust, smoke to code](logo.jpg "Rust Belt Rust")

Rust Belt Rust 2018 will take place on Friday October 19 & Saturday October 20 at [Wyndham Garden, Ann Arbor, Michigan](https://www.wyndhamhotels.com/wyndham-garden/ann-arbor-michigan/wyndham-garden-ann-arbor/overview)

## Tickets

<a class="call-to-action" href="https://www.eventbrite.com/e/rust-belt-rust-conference-2018-registration-47481792319">Buy Tickets</a>

Early bird tickets are $175; $50 for students. The RustBridge workshop on day 1 for people who are underrepresented in tech is free (a $20 deposit holds your spot and will be refunded to you when you arrive). If you are attending RustBridge as well as day 2, please choose a Day 2 early bird or student ticket as appropriate. The early bird prices will expire when we announce the speaker lineup around August 17.

## Sponsors

Thank you to our wonderful [sponsors](sponsors); this conference wouldn't be possible without their support!

## Lodging

We have reduced hotel rate at the Wyndham Garden Ann Arbor. Use the block code **RBR** when making a reservation or simply say you are with the *Rust Belt Rust* block. You can either call the hotel or book on line.

## Childcare

If you will need childcare, please email [organizers@rust-belt-rust.com](mailto:organizers@rust-belt-rust.com) as soon as possible. We will work with you to find a licensed childcare provider at your hotel and will provide a stipend to cover the cost.

## Slack

We have a [Slack instance](https://rust-belt-rust-slack.herokuapp.com/) that you are welcome to join. Potential topics include things to do around Ann Arbor before, during, and after the conference; your Rust projects; ideas for the unconference; or just arranging meals!

## Conduct

Rust Belt Rust is dedicated to providing a harassment-free conference experience for everyone. Please see our [code of conduct](conduct) for more details.
