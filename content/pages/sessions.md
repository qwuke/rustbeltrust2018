+++
title = "Sessions"
path = "sessions"
+++
# Rust Belt Rust Conference Sessions

## Day one

The first day of Rust Belt Rust, Friday, October 19, will be workshops. We plan to have several workshops to choose from, some half day and some full day.

## Day two

The second day, Saturday, October 20, will alternate between short, half-hour talks and half-hour breaks for conference attendees to talk with each other. There will also be a set of five minute lightning talks.

## Call for proposals

*Our call for proposals is open!*

<a class="call-to-action" href="https://cfp.rust-belt-rust.com/">Propose a Talk</a>

Submissions are open until Mon July 30, 2018 at 5pm ET. All submitters will be notified about whether they have been selected or not by the end of August at the latest.
